"use strict";

let arr = ["один", "два", "три", "чотири", "п'ять", "шість", "сім", "вісім", "дев'ять", "десять"
    , "одинадцять", "дванадцять", "тринадцять", "чотирнадцять", "п'ятнатцять", "шістнадцять", "сімнадцять"
    , "вісімнадцять", "дев'ятнадцять", "двадцять"
    , "двадцять один", "двадцять два", "двадцять три", "двадцять чотири", "двадцять п'ять"
    , "двадцять шість", "двадцять сім", "двадцять вісім", "двадцять дев'ять", "тридцять"
    , "тридцять один", "тридцять два", "тридцять три", "тридцять чотири", "тридцять п'ять"
    , "тридцять шість", "тридцять сім", "тридцять вісім", "тридцять дев'ять", "сорок"
    , "сорок один", "сорок два", "сорок три", "сорок чотири", "сорок п'ять", "сорок шість"
    , "сорок сім", "сорок вісім", "сорок дев'ять", "п'ятдесят", "п'ятдесят один", "п'ятдесят два"
    , "п'ятдесят три", "п'ятдесят чотири", "п'ятдесят п'ять", "п'ятдесят шість", "п'ятдесят сім"
    , "п'ятдесят вісім", "п'ятдесят дев'ять", "шістдесят", "шістдесят один", "шістдесят два"
    , "шістдесят три", "шістдесят чотири", "шістдесят п'ять", "шістдесят шість", "шістдесят сім"
    , "шістдесят вісім", "шістдесят дев'ять"];

function rezult() {
    let num = parseInt(intNum.value);
    convertNum.value = `${arr[num - 1]}`
}

function toCamelCase() {
    let kebab = kebabText.value;
    let list = kebab.split('-');
    for (let i = 1; i < list.length; i++) {
        let word = list[i];
        word = word[0].toUpperCase() + word.slice(1);
        list[i] = word;
    }
    camelText.value = list.join('');
}

function toKebabCase(){
    let camel = camelText1.value;
    let charList = [];
    for(let i = 0;i < camel.length; i++){
        if(camel[i] === camel[i].toUpperCase()){
            charList.push(`-${camel[i].toLowerCase()}`);
        }else {
            charList.push(camel[i])
        }
    }
    kebabText1.value = charList.join('');
}

function calc() {
    let input = inputString.value;
    let nums = input.split(/\D+/);
    let operation = input.split(/[\d ]+/);
    switch (operation[1]) {
        case '+':
            result.value = parseInt(nums[0]) + parseInt(nums[1]);
            break;
        case '-':
            result.value = parseInt(nums[0]) - parseInt(nums[1]);
            break;
        case '*':
            result.value = parseInt(nums[0]) * parseInt(nums[1]);
            break;
        case '/':
            result.value = parseInt(nums[0]) / parseInt(nums[1]);
            break;
    }
}

function corectDate(){
    let text = textar.value;
    let textList = text.split(' ');
    const regex = /^[1-2]\d{3}\/[0-1]\d\/\d{2}$/;

    for (let i = 0;i<textList.length;i++)
    {
        if(regex.exec(textList[i])){
            textList[i] = textList[i].split("/").reverse().join(".");
        }
    }
    textar.value = textList.join(' ');
}