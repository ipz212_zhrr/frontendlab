let obj = {
    className: 'open menu menu open'
}

function addClass(object = obj, string){
    let str = object.className;
    if(str.indexOf(string) === -1) {
        object.className = `${str} ${string}`;
    }
    alert(object.className)
}

function deleteClass(object = obj, string){
    let str = object.className;
    if(str.indexOf(string) !== -1) {
        str = str.replaceAll(`${string}`,``);
        str = str.split('  ').join(' ').trim();
        object.className = str;
    }
    alert(object.className)
}

function Dog(name, breed, age)
{
    this.name = name;
    this.breed = breed;
    this.age = age;
    this.dogSay = function() {
        if (this.age < 1)
            alert('Тяф');
        if (this.age >= 1 && this.age <= 3)
            alert('Гав');
        if (this.age > 3)
            alert('Грр');
    }
}


function callDog(name,breed,age){
    let dog1 = new Dog(name,breed,age);
    dog1.dogSay();
}