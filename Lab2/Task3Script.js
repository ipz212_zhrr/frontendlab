function unique(arr = strings) {
    let temp = []
    for(let i = 0; i < arr.length; i++){
        if (temp.indexOf(arr[i])<0) temp.push(arr[i]);
    }
    return temp
}

let strings = ["C++", "C#", "C++", "C#",
    "C", "C++", "JavaScript", "C++", "JavaScript"
];

function Subject(name, points, result){
    this.name = name;
    this.points = points;
    this.result = result;

}

let sesion = [
    new Subject('Математика',90,'зараховано'),
    new Subject('Історія',80,'зараховано'),
    new Subject('Хореографія',50,'незараховано'),
    new Subject('Географія',76,'зараховано')
]

function comparer(subject1, subject2){
    if(subject1.points < subject2.points) return 1;
    if (subject1.points === subject2.points) return 0;
    if (subject1.points > subject2.points) return -1;
}

function print_list(list = sesion){
    list.sort(comparer);
    for (let i=0;i<list.length;i++){
        alert(`Subject:${list[i].name} Points:${list[i].points} Result:${list[i].result}` )
    }
}

function get_result(points){
    if(points<60){
        return 'незараховано'
    }else {
        return 'зараховано'
    }
}

function get_index(name){
    for (let i=0;i<sesion.length;i++){
        if(sesion[i].name === name) return i;
    }
    return -1;
}

function add_subject(name,point){
    sesion.splice(get_index(name),1)
    sesion.push(new Subject(name,point,get_result(point)))
}

function avg_points(){
    let i = 0;
    let sum =0;
    for (i;i<sesion.length;i++){
        sum += sesion[i].points;
    }
    return sum/i;
}

function fall_count(){
    let count = 0;
    for (let i=0;i<sesion.length;i++)
        if(sesion[i].points<60) count++;
    return count;
}

function best_subject(){
    sesion.sort(comparer);
    return sesion[0].name;
}

function find_subjects(points){
    let temp = []
    for (let i=0;i<sesion.length;i++){
        if(sesion[i].points === points) temp.push(sesion[i]);
    }
    return temp;
}