let div = document.querySelector('#main');
let but = document.querySelector('#sub');

but.addEventListener('click', () => {
    let str = document.querySelector('#text').value;
    let size = document.querySelector('#size').value;
    newFontSize(str, size);
})

function newFontSize(str, fontSize) {
    div.style.fontSize = fontSize + "px";
    div.innerHTML = str;
}
