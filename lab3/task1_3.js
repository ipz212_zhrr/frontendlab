let but = document.querySelector('#but');

but.addEventListener('click', () => {
    let n = document.querySelector("#num").value;
    createHeaders(n);
});

function createHeaders(n, element = document.body) {
    for (let i = 0; i < n; i++) {
        let h = document.createElement('h2');
        element.appendChild(h);
        h.innerHTML = `Header${i + 1}`;
    }
}