function func(...args) {
    let len = args.length;
    let sum = 0;
    let s = '';
    for (let i = 0; i < len; i++) {
        if (parseInt(args[i])) {
            sum += parseInt(args[i]);
        }
        else s += `${args[i]} `;
    }
    console.log(`sum = ${sum}`);
    console.log(`str = ${s}`);
    return `Sum = ${sum};  Str = ${s}`;
}

div = document.querySelector('#div');
let result = func(1, 2,"world", 3, 4, "qwerty", "hello");
div.innerHTML = result;


