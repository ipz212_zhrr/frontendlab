let but = document.querySelector('#button');

let month = [
    'січня',
    'лютого',
    'березня',
    'квітня',
    'травня',
    'червня',
    'липня',
    'серпня',
    'вересня',
    'жовтня',
    'листопада',
    'грудня'];
let day = ['неділя',
    'понеділок',
    'вівторок',
    'середа',
    'четвер',
    'п\'ятниця',
    'субота'];

let td = document.querySelector('#curDate');
but.addEventListener('click', ()=>{
    let curDate = new Date();
    let str = `Дата: ${curDate.getDate()} ${month[curDate.getMonth()]} 
    ${curDate.getFullYear()} року<br>День: ${day[curDate.getDay()]}<br>Час: ${curDate.getHours()}:${curDate.getMinutes()}:${curDate.getSeconds()}`;
    td.innerHTML = str;
})



