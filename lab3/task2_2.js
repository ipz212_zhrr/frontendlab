let but = document.querySelector('#but');

but.addEventListener('click', () => {
    let date = document.querySelector('#date').value;

    if (!(date.indexOf('.') === -1)) {
        date = date.replaceAll('.', '-');
        console.log(`(.)${date}`);
    } else if (!(date.indexOf('/') === -1)) {
        date = date.replaceAll('/', '-');
        console.log(`(/)${date}`);
    }
    let arr = date.split('-');
    console.log(arr);
    let tmp = arr[2];
    arr[2] = arr[0];
    arr[0] = tmp;
    console.log(arr);

    date = arr.join('-');
    console.log(date)
    date = new Date(Date.parse(date));
    console.log(date)

    let str = getStr(date);
    console.log(str);
    let res = document.querySelector('#result');
    res.innerHTML = str;
})

function getStr(date) {
    let k = new Date() - date;
    console.log(k);
    if (k < 86400000)
        return "Сьогодні";
    else if (k >= 86400000 && k < 172800000)
        return "Вчора";
    else if (k >= 604800000 && k < 691200000)
        return "Тиждень тому";
    else if (k >= 518400000 && k < 604800000)
        return "6 днів тому";
    else if (k >= 432000000 && k < 518400000)
        return "5 днів тому";
    else if (k >= 345600000 && k < 432000000)
        return "4 дні тому";
    else if (k >= 259200000 && k < 345600000)
        return "3 дні тому";
    else if (k >= 172800000 && k < 259200000)
        return "2 дні тому";
    else if (k >= 31557600000 && k < 34187400000)
        return "Рік тому";
    else return `${date.getFullYear()}.${date.getMonth() + 1}.${date.getDate()}`;
}



