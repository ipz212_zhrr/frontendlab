let but = document.querySelector('#button');

but.addEventListener('click', ()=> {
    let text = document.querySelector('#text').value;
    let arr = findDates(text);
    console.log(arr);
    for (let i=0; i<arr.length;i++){
        let div = document.createElement('div')
        div.innerHTML = arr[i];
        document.body.appendChild(div);
    }
})

function findDates(text){
    let reg = /\d{4}-\d{2}-\d{2}/g;
    let arr = text.match(reg);
    let result =[];
    for (let i = 0; i < arr.length; i++){
        if (checkDate(arr[i]))
            result.push(arr[i]);
    }
    return result;
}

function checkDate(str){
    let arr = str.split('-');
    let now = new Date();
    let res = true;
    if((arr[0]>=1950 && arr[0]<=now.getFullYear()) && (arr[1]>=1 && arr[1]<=12) && (arr[2]>=1 && arr[2]<=31)){
        if (arr[1]===2 && arr[2]>28)
            res = false;
        if (arr[1]<=7 && arr[1]%2===0 && arr[2]>30){
            res = false;
        }
        if (arr[1]>=8 && arr[1]%2!==0 && arr[2]>30){
            res = false;
        }
    } else res = false;
    return res;
}
