let table = document.querySelector('#table');

function createTableBody(value, resValue, table) {
    let tr = document.createElement('tr');
    let td1 = document.createElement('td');
    let td2 = document.createElement('td');
    td1.innerHTML = value;
    td2.innerHTML = resValue;
    tr.appendChild(td1);
    tr.appendChild(td2);

    table.appendChild(tr);
}

let but = document.querySelector('#button');
but.addEventListener('click', () => {
    let val = document.querySelector('#value').value;
    createTableBody(val,toCamelCase(val),table);
})

function toCamelCase(str) {
    if ( !(/[_-]/).test(str) ) return str;
    return str.toLowerCase().replace(/[-_][a-z0-9]/g,
        (group) => group.slice(-1).toUpperCase( ));

}