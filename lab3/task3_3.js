let table = document.querySelector('#table');
let but = document.querySelector('#button');

function createTableBody(value, resValue) {
    let tr = document.createElement('tr');
    let td1 = document.createElement('td');
    let td2 = document.createElement('td');
    td1.innerHTML = value;
    td2.innerHTML = resValue;
    tr.appendChild(td1);
    tr.appendChild(td2);

    table.appendChild(tr);
}

but.addEventListener('click', () => {
    let val = document.querySelector('#value').value;
    createTableBody(val,toSnakeCase(val), table);
})

function toSnakeCase(str) {
    return str.split('').map((character) => {
        if (character === character.toUpperCase()) {
            return '_' + character.toLowerCase();
        } else {
            return character;
        }
    })
        .join('');
}