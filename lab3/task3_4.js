
but.addEventListener('click', () => {
    let val = document.querySelector('#text').value;
    let arr = findHexColor(val);
    for (let i = 0; i < arr.length; i++) {
        let div = document.createElement('div')
        div.innerHTML = arr[i];
        document.body.appendChild(div);
    }
})

function findHexColor(text) {
    let reg = /\#([A-Za-z0-9][A-Za-z0-9][A-Za-z0-9][A-Za-z0-9][A-Za-z0-9][A-Za-z0-9])|\#([A-Za-z0-9][A-Za-z0-9][A-Za-z0-9])/g;
    let arr = text.match(reg);
    let result = [];
    for (let i = 0; i < arr.length; i++) {
        result.push(arr[i]);
    }
    return result;
}


