const task1 = document.querySelector("#OK");
task1.addEventListener("click", (event) => {
    let languages = ['ua', 'en', 'de'];
    let results = document.querySelector("form[name='task1'] .resultTask1");
    results.innerHTML = '';
    for (let i = 0; i < languages.length; i++) {
        let checkedBox = document.getElementById(languages[i]);
        if (checkedBox.checked) {
            results.innerHTML += languages[i] + " ";
        }
    }
})