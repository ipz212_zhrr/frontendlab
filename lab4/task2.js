let resultsTask2 = document.querySelector("#resultsTask2");
let emails = document.querySelectorAll(".email")
for (let email of emails) {
    email.addEventListener("click", function () {
        let label = email.nextElementSibling.innerHTML;
        if (email.checked === true)
            resultsTask2.innerHTML += label;
        else
            resultsTask2.innerHTML = resultsTask2.innerHTML.replace(label, "");
    })
}