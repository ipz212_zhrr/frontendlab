let Task3Count = document.querySelector("#Task3Count");
let radioButtons = document.querySelector("#formTask3");
let btnTask3 = document.querySelector("#btnTask3");
let results3 = document.querySelector("#resultsTask3");
let percentage = 0;
let count3 = 0;
mathQuiz();
btnTask3.addEventListener("click", mathQuiz);
function radios(value, answer) {
    let label = document.createElement("label");
    let input = document.createElement("input");
    let div = document.createElement("div");
    input.type = "radio";
    input.classList.add('task3');
    input.value = value;
    label.innerHTML = value;
    label.width = '100px';
    label.appendChild(input);
    div.appendChild(label);
    radioButtons.appendChild(div);
    input.addEventListener("click", function () {
        if (Number(input.value) === answer) {
            percentage++;
            div.style.backgroundColor = "green";
        } else {
            div.style.backgroundColor = "red";
            results3.style.color = 'red';
            results3.innerHTML = `Wrong! Correct answer is "${answer}"`
        }
        for (let elem of document.querySelectorAll(".task3"))
            elem.disabled = true;
        Task3Count.innerHTML = `${percentage * 10}% answered correctly.<br>Question №${count3} out of 10:<br>`;
        if (count3 === 11) {
            count3 = 0;
            percentage = 0;
        }
    });
}
function getRandomArbitrary(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}
function mathQuiz() {
    if (count3 === 10) {
        count3 = 0;
        percentage = 0;
    }
    count3++;
    results3.innerHTML = '';
    Task3Count.innerHTML = `${percentage * 10}% answered correctly.<br> Question №${count3} out of 10:<br>`;
    for (let elem of document.querySelectorAll(".task3"))
        elem.parentNode.remove();
    let first = getRandomArbitrary(2, 10);
    let second = getRandomArbitrary(2, 10);
    let answerRadio = getRandomArbitrary(0, 4);
    let text = document.querySelector("#Task3span")
    let res = first * second;
    text.innerHTML = first + " x " + second + " = ";
    for (let i = 0; i < 4; i++) {
        if (i === answerRadio)
            radios(res, res);
        else
            radios(getRandomArbitrary((res - 10), (res - 5)), res);
    }
}