let infoBar = document.querySelector('#infoBar');
let btnNext = document.querySelector("#btnNext");
let questionBar = document.querySelector("#questionBar");
let txtAnswer = document.querySelector("#txtAnswer");
let btnCheck = document.querySelector("#btnCheck");
let answerBar = document.querySelector("#answerBar");
let first;
let second;
let countQuiz = 0;
let correctCount = 0;
generateNext();
btnNext.addEventListener("click", generateNext);
btnCheck.addEventListener("click", checkAnswer);
function checkAnswer() {
    btnCheck.disabled = true;
    if (Number(txtAnswer.value) === first * second)
        correctCount++;
    else
        answerBar.innerHTML = `Wrong! The answer is ${first * second}.`;
    infoBar.innerHTML = `${correctCount} correct answers. Question #${countQuiz}out of 10.`;
}
function generateNext() {
    if (countQuiz !== 0 && !btnCheck.disabled) {
        let correctNow = correctCount;
        checkAnswer();
        if (correctNow === correctCount)
            return;
    }
    btnCheck.disabled = false;
    txtAnswer.value = '';
    answerBar.innerHTML = "";
    countQuiz++;
    if (countQuiz === 11) {
        countQuiz = 1;
        correctCount = 0;
    }
    infoBar.innerHTML = `${correctCount} correct answers. Question #${countQuiz}out of 10.`
    first = getRandomArbitrary(2, 10);
    second = getRandomArbitrary(2, 10);
    questionBar.innerHTML = `${first} x ${second} = `;
}
function getRandomArbitrary(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}