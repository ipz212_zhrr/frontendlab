let forms = document.querySelectorAll(".forms");
for (let form of forms) {
    form.parentNode.style.border = "1px solid #9494ff"
    form.parentNode.style.margin = "10px 0"
    form.style.margin = "10px"
    form.addEventListener("focus", function () {
        form.parentNode.style.backgroundColor = "#92bbe0";
    })
    form.addEventListener("blur", function () {
        form.parentNode.style.backgroundColor = "white";
    })
}