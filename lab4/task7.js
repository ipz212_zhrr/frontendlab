let ranges = document.querySelectorAll(".range")
let square = document.createElement("div")
let field = document.querySelector('#field')
square.style.width = "100px"
square.style.height = "70px"
square.style.rotate = "60deg"
square.style.backgroundColor = "#c65f5f"
field.appendChild(square)
for (let range of ranges) {
    range.oninput = function () {
        range.nextElementSibling.innerHTML = range.value;
        if (range.name === "width")
            square.style.width = `${range.value}px`
        if (range.name === "height")
            square.style.height = `${range.value}px`
        if (range.name === "rotate")
            square.style.rotate = `${range.value}deg`
    }
}