let butResize = document.querySelector("#butResize");
butResize.addEventListener("click", function () {
    let task8Form = document.querySelector('#task8');
    task8Form.classList.toggle('resize');
    butResize.nextElementSibling.classList.toggle('enlarge');
})