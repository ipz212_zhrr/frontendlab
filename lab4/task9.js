let btnTask9 = document.querySelectorAll(".btnTask9")
let orders = document.querySelector("#basket").querySelectorAll('td')
for (let btn of btnTask9) {
    btn.addEventListener('click', function () {
        for (let order of orders) {
            if (order.innerHTML ===
                btn.parentElement.previousElementSibling.innerHTML) {
                order.nextElementSibling.innerHTML =
                    Number(order.nextElementSibling.innerHTML) + 1;
            }
        }
    })
}