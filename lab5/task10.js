let button = document.querySelector("button");
let container = document.querySelector(".container");
let leftBorder = parseInt(getOffset(container).left);
let topBorder = parseInt(getOffset(container).top);
let rightBorder = parseInt(leftBorder + container.offsetWidth - button.offsetWidth - 4);
let bottomBorder = parseInt(topBorder + container.offsetHeight - button.offsetHeight -4);

button.addEventListener('mouseenter', (event)=>{
    let newX = getRandomInt(leftBorder, rightBorder);
    let newY = getRandomInt(topBorder, bottomBorder);
    console.log(event.target);
    button.style.left = newX + "px";
    button.style.top = newY + "px";
})

button.addEventListener('click', (event)=>{
    container.innerHTML += "<p id = 'win'>YOU CAUGHT!</p>";
})
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function getOffset(el) {
    const rect = el.getBoundingClientRect();
    return {
        left: rect.left + window.scrollX,
        top: rect.top + window.scrollY
    };
}