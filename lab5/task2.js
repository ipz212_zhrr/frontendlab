let text = document.querySelector("textarea");
let table = document.querySelector("table");
let tr = document.querySelectorAll('tr');
let pattern;
let trIndex = 0;
text.addEventListener('input', (event)=>{
    pattern = text.value;
    let currentSymb = text.value[text.value.length-1]
    if(currentSymb === '0'){
        tr[trIndex].innerHTML+="<td class = 'zero'></td>";
    }
    else if(currentSymb === '1'){
        tr[trIndex].innerHTML+="<td class = 'one'></td>";
    }
});

text.addEventListener('keypress', (event)=>{
    if(event.key === "Enter"){
        table.innerHTML += "<tr>";
        trIndex++;
        tr = document.querySelectorAll('tr');
    }
});
