let colorPick = document.querySelectorAll("td");
let table = document.querySelector("table");
let block = document.querySelector('#block');
let isColorPicked = [];
let colorArr = [];
let countColor = 0;
for(let i of colorPick){
    isColorPicked.push(false);
    i.abbr = "0";
}

table.addEventListener('click', (event)=>{
    const target = event.target.closest('td');
    if(target.abbr === "0" && target.abbr!=null){
        countColor++;
        if(countColor === 1){
            block.style.backgroundColor = target.style.backgroundColor;
            block.style.backgroundImage = "";
        }
        target.abbr = "1";
        colorArr.push(target.style.backgroundColor);
        target.style.border = "3px solid black";
    }
    else{
        countColor--;
        target.abbr = "0";
        if(countColor === 0){
            block.style.backgroundColor = "white";
            block.style.backgroundImage = "";
        }
        else if(countColor ===1){
            for(let i of colorPick){
                if(i.abbr === "1"){
                    block.style.backgroundColor = i.style.backgroundColor;
                    block.style.backgroundImage = "";
                }
            }
        }
        let index = colorArr.indexOf(target.style.backgroundColor)
        colorArr.splice(index, 1);
        target.style.border = "1px solid black";
    }
    if(countColor !==0) {
        console.log(countColor)
        changeGradient(colorArr);
    }
})
 function changeGradient(arr){
     let styleStr = "linear-gradient(to top";

     for(let j of arr){
         styleStr+= "," + j ;
     }
     styleStr+=")";
     block.style.backgroundImage = styleStr;
 }