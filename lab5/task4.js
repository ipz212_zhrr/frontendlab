let container = document.querySelector(".container");

container.addEventListener('click', (event)=>{
   let target = event.target;
    console.log(target.className)
   if(target.closest("img") && target.className === "changeBtn"){
        let block = target.parentElement.parentElement;
        let textArea = document.createElement('textarea');;
        let p;
        p = target.parentElement.parentElement.lastElementChild;
        let text = p.innerHTML;
        p.style.display = "none";
        textArea.innerHTML = text;
        block.appendChild(textArea);
        console.log(block);
       textArea.addEventListener('focusout', (event)=>{
           console.log(textArea.value)
           p.innerHTML = textArea.value;
           textArea.style.display = "none";
           p.style.display = "block";
       });
   }
   else if(target.closest("img") && target.className === "exitBtn") {
       container.removeChild(target.parentElement.parentElement);
   }
});

