let canvasTable = document.querySelector(".canvas");
let colorTable = document.querySelector(".palette");
let colorPick = document.querySelectorAll("td");
let currentColor;
for(let i =0; i<8;i++){
    canvasTable.innerHTML+="<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
}

colorTable.addEventListener('click', (event)=>{
    const target = event.target.closest('td');
    changeActiveColor();
    target.style.border = "3px solid black";
    currentColor = target.style.backgroundColor;
});

canvasTable.addEventListener('click', (event) =>{
    const target = event.target.closest('td');
    target.style.backgroundColor = currentColor;
});

function changeActiveColor(){
    for( let i of colorPick){
        i.style.border = "1px solid black";
    }
}