let cardFront = document.querySelector('#front');
let cardBack = document.querySelector("#back");
let container = document.querySelector(".container");
let front = false;
let back = true;
let isCardBack = false;


cardFront.addEventListener('click', (event)=>{
    event.target.className = 'flipFrontCard';
    console.log(event.target);
});
cardBack.addEventListener('click',(event)=>{
    event.target.className = 'flipBackCardReversed';
});
cardFront.addEventListener('animationstart', (event)=>{

    console.log("front started")
});
cardBack.addEventListener('animationstart', (event)=>{
    console.log("back started")
});
cardFront.addEventListener('animationend', (event)=>{
    if(!front) {
        event.target.style.display = "none";
        front = true;
        cardBack.style.display = "block";
        cardBack.className = "flipBackCard";
    }
    else{
        event.target.style.display = "block";
        front = false;
    }
    console.log(front)
    console.log("front ended")

});

cardBack.addEventListener('animationend', (event)=>{
    if(!back) {
        event.target.style.display = "none";
        back = true;
        cardFront.style.display = "block";
        cardFront.className = "flipFrontCardReversed";
    }
    else{
        event.target.style.display = "block";
        back = false;
    }
    console.log("back ended")

});
