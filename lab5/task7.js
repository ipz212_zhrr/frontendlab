let container = document.querySelector('.container');
let blocks = document.querySelectorAll('.block');

let coordX, coordY;
let block = null;
let zIndex = 0;
let contTop = getOffset(container).top + 2;
let contLeft = getOffset(container).left + 2;
let contWidth = container.offsetWidth;
let contHeight = container.offsetHeight;
let targetWidth;
let targetHeight;

document.body.addEventListener('mousedown', (event)=>{
   const target = event.target.closest('.block');
   if(target !=null && target.classList.contains('block')){
       coordX = event.pageX;
       coordY = event.pageY;
       block = target;
       targetWidth = target.offsetWidth;
       targetHeight = target.offsetHeight;
       target.style.zIndex = ++zIndex;
   }
});
document.documentElement.addEventListener('mousemove', (event)=>{
   if(event.buttons & 1 === 1){
       let dx = event.pageX - coordX;
       let dy= event.pageY - coordY;
       let rightBorder = contLeft + contWidth - targetWidth - 3;
       let bottomBorder = contTop + contHeight - targetHeight - 3;
       coordX = event.pageX;
       coordY = event.pageY;
       if(getOffset(event.target).left>contLeft && getOffset(event.target).left < rightBorder) {
           event.target.style.left = parseInt(getComputedStyle(event.target).left) + dx + "px";
       }
       else if(getOffset(event.target).left<=contLeft){
           if(dx>0) {
               event.target.style.left = parseInt(getComputedStyle(event.target).left) + dx + "px";
            }
       }
       else if(getOffset(event.target).left >= rightBorder){
           if(dx<0) {
               event.target.style.left = parseInt(getComputedStyle(event.target).left) + dx + "px";
           }
       }
       if(getOffset(event.target).top > contTop&&getOffset(event.target).top<bottomBorder ) {
           event.target.style.top = parseInt(getComputedStyle(event.target).top) + dy + "px";
       }
       else if(getOffset(event.target).top <= contTop){
           if(dy>=0){
               event.target.style.top = parseInt(getComputedStyle(event.target).top) + dy + "px";
           }
       }
       else if(getOffset(event.target).top >=bottomBorder){
           if(dy<0) {
               event.target.style.top = parseInt(getComputedStyle(event.target).top) + dy + "px";
           }
       }

   }
});


function getOffset(el) {
    const rect = el.getBoundingClientRect();
    return {
        left: rect.left + window.scrollX,
        top: rect.top + window.scrollY
    };
}