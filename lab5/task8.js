let container = document.querySelector(".container");
let sendBtn = document.querySelector("#btn");
let name = document.querySelector("input[name='name']");
let comment = document.querySelector("#comment");
let table;

sendBtn.addEventListener('click', (event)=>{
    if(name.value !== "" && comment.value !== ""){
        let date = new Date();
        let dateStr = date.toDateString()
        if(date.getMinutes()< 10){
            dateStr += " " + date.getHours() + ":0" + date.getMinutes();
        }
        else{
            dateStr += " " + date.getHours() + ":" + date.getMinutes();
        }
        table = document.createElement("table");
        let trName = document.createElement("tr");
        let trComment = document.createElement("tr");
        trName.innerHTML = "<td>" +name.value+"</td><td style = 'text-align: right;'>"+dateStr+"</td>";
        trComment.innerHTML = "<td colspan='2'>" +comment.value+"</td>";
        table.appendChild(trName);
        table.appendChild(trComment);
        container.appendChild(table);
    }
    else{
        let errorDiv = document.querySelector(".error");
        errorDiv.style.color = "white";
        errorDiv.style.border = "2px solid darkred";
        errorDiv.style.backgroundColor = "red";
        errorDiv.style.borderRadius = "2px";
        errorDiv.style.width = "200px";
        errorDiv.style.padding = "5px";
        errorDiv.style.textAlign = "center";
        errorDiv.innerHTML = "Error! Please, fill all fields!"
    }
});
