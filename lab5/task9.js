let bookList = document.querySelector("ol");
let listItems =document.querySelectorAll("li");

bookList.addEventListener('click', (event)=>{
   let target = event.target;
   if(target!=null && target.closest("li")) {
       for(let i of listItems){
           i.style.backgroundColor = "white";
       }
       target.style.backgroundColor = "lightgreen";
   }
});