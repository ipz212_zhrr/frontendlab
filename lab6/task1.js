let rightArrow = document.querySelector("#rightArrow");
let leftArrow = document.querySelector("#leftArrow");
let textArea = document.querySelector('textarea');
let currentChange = getCookie('countChanges');
textArea.addEventListener('focusout', (event)=>{
    if(getCookie('countChanges') === ""){
        setCookie("countChanges", 0, 20);
        setCookie("change" + getCookie('countChange'), textArea.value, 20);
        currentChange = getCookie('countChanges');
    }
    else{
        setCookie("countChanges", parseInt(getCookie('countChanges'))+1, 20);
        currentChange = getCookie('countChanges');
    }
    setCookie("change"+currentChange, textArea.value, 20);
});

leftArrow.addEventListener('click', (event)=>{
    currentChange--;
    textArea.value = getCookie("change"+currentChange);
});
rightArrow.addEventListener('click', (event)=>{
    currentChange++;
    textArea.value = getCookie("change"+currentChange);
});


function setCookie(name, value, days) {
    const d = new Date();
    d.setTime(d.getTime() + (days*24*60*60*1000));
    let expires = "expires="+ d.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
}
function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}