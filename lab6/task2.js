let nameText = document.querySelector('input[type="text"]');
let textArea = document.querySelector("textarea");
let checkBoxes = document.querySelectorAll('input[type=checkbox]');
let radios = document.querySelectorAll('input[type=radio]');
let select = document.querySelector('select');
let options = select.options;
initFormElements();
if(localStorage.getItem('select') == null) {
    for (let i of options) {
        if (i.selected) {
            localStorage.setItem('select', i.value);
            console.log(localStorage.getItem('select'));
        }
    }
}

nameText.addEventListener('change', (event)=>{
    if(nameText.value !== ""){
        localStorage.setItem('nameText', nameText.value);
        console.log( localStorage.getItem("nameText"))
    }
    else{
        localStorage.setItem('nameText', "");

    }
});
textArea.addEventListener('change', (event)=>{
    if(textArea.value!==""){
        localStorage.setItem('textArea', textArea.value);
    }
    else{
        localStorage.setItem('textArea', "");
    }
});
let checkedBoxes = [];
for (let checkBox of checkBoxes) {
    checkBox.addEventListener('change', (event)=>{
        if(checkBox.checked){
            checkedBoxes.push(checkBox.value);
            console.log(checkedBoxes);
        }
        else{
            let index = checkedBoxes.indexOf(checkBox.value);
            if (index > -1) {
                checkedBoxes.splice(index, 1);
            }
        }
        localStorage.setItem("checkBoxes", JSON.stringify(checkedBoxes));
        console.log(localStorage.getItem("checkBoxes"));
    });
}

for (let radio of radios) {

    radio.addEventListener('change', (event)=>{
       if(radio.checked){
           localStorage.setItem('radio', radio.value);
       }
    });
}
select.addEventListener('change', (event)=>{
    for(let i of options){
        if(i.selected){
            localStorage.setItem('select', i.value);
        }
    }
});


function initFormElements(){
    if(localStorage.getItem("nameText")!=null){
        nameText.value = localStorage.getItem("nameText");
    }
    if(localStorage.getItem("textArea")!= null){
        textArea.value = localStorage.getItem("textArea");
    }
    if(localStorage.getItem("checkBoxes") !=null){
        let cBoxes = JSON.parse(localStorage.getItem("checkBoxes"));
        for(let i of cBoxes){
            for(let j of checkBoxes){
                if(j.value === i){
                    j.checked = true;
                }
            }
        }
    }
    if(localStorage.getItem("radio")!=null){
        for(let i of radios){
            if(i.value === localStorage.getItem("radio")){
                i.checked = true;
            }
        }
    }
    if(localStorage.getItem("select")!=null){
        for(let i of select.options){
            if(i.value === localStorage.getItem("select")){
                i.selected = true;
            }
        }
    }
}

