let inputText = document.querySelector("input[type=text]");
let showButton = document.querySelector("input[type=button]");
let hintLabel = document.querySelector("#hintMessage");
let imagesContainer = document.querySelector(".images");
let fullImageContainer = document.querySelector(".fullImageContainer");
let imagesArr = document.querySelectorAll("img")

showButton.addEventListener('click', ()=>{
   if(inputText.value !== "" || inputText.value != null){
       if(isJsonString(inputText.value)) {
           hintLabel.innerHTML = "OK";
           hintLabel.style.color = "green";
           let fileNames = JSON.parse(inputText.value);
            addImages(fileNames);
           console.log(fileNames);
       }
       else{
           hintLabel.innerHTML = "Невірний формат JSON!";
           hintLabel.style.color = "red";
       }
   }
});

imagesContainer.addEventListener('click', (event)=>{
    let target = event.target;
    fullImageContainer.innerHTML = "";
    let fullImage = target.cloneNode(true);
    fullImage.className = "fullImage";
    fullImageContainer.appendChild(fullImage);
});

function addImages(imageNames){
    for(let i of imageNames){
        let img = document.createElement("img");
        img.src = "files\\" + i;
        imagesContainer.appendChild(img);
    }
}

function isJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}