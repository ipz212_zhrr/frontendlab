class Student{
    constructor(surname, name, marks) {
        this.surname = surname;
        this.name = name;
        this.marks = [];
        for(let i  = 0; i<marks.length; i++){
            this.marks[i] = marks[i];
        }
    }
    clone(){
        return new Student(this.surname, this.name, ()=>{
            let newMarks = [];
            for(let i  = 0; i<this.marks.length; i++){
                newMarks[i] = this.marks[i];
            }
            return newMarks;
        })
    }
}
class ListOfStudent{

    constructor(studentsArray) {
        this.students = [];
        for (let i = 0; i < studentsArray.length; i++) {
            this.students[i] = studentsArray[i].clone();
        }
    }

    getTableList(){
        let table = document.createElement("table");
        let th = document.createElement("tr");
        th.innerHTML = "<td>Name</td><td>Surname</td><td>Math</td><td>History</td><td>JS</td>";
        table.appendChild(th);
        let tr;
        for(let i of studentArr){
            tr = document.createElement("tr");
            tr.innerHTML = "<td>"+i.name+"</td><td>"+i.surname+"</td>";
            for(let j of i.marks){
                tr.innerHTML += "<td>"+j+"</td>";
            }
            table.appendChild(tr);
        }
        table.style.border = "2px solid lightgrey";
        table.style.borderCollapse = "collapse";
        table.style.padding = "3px";
        table.style.textAlign = "center";
        return table;
    }
}
class StylesTable extends ListOfStudent{
    constructor(studentsArray) {
        super(studentsArray);
        this.students = studentsArray;
    }
    getStyles(){
        return this.getTableList().style.cssText;
    }
    getTableList(){
        let table =  super.getTableList();
        let tableChild = table.childNodes;
        let tableHead = table.firstChild;
        let tableHeadStyle = tableHead.style;
        tableHeadStyle.backgroundColor = "green";
        tableHeadStyle.fontWeight = "bold";
        tableHeadStyle.color = "white";
        for(let i of tableChild){
            if(i !== tableHead) {
                i.style.color = "green";
                i.style.textAlign = "left";
            }
        }
        return table;
    }

    getAvg(){
        let table =  this.getTableList();
        let tableChild = [];
        let tableHead = table.firstChild;
        for(let i of table.childNodes){
            if(i!== tableHead){
                tableChild.push(i);
            }
        }
        let headAvg = document.createElement("td");
        headAvg.innerHTML = "Avg";
        tableHead.appendChild(headAvg);
        for(let i =0; i<this.students.length; i++){
            let tdAvg = document.createElement("td");
            let sum = 0;
            for(let j of this.students[i].marks){
                sum+=j;
            }
            tdAvg.innerHTML = Number(sum / this.students[i].marks.length).toFixed(2);
            tableChild[i].appendChild(tdAvg);

        }
        return table;
    }
    getTotalAvg(){
        let totalSum = 0;
        let count = 0;
        for(let i =0; i<this.students.length; i++){
            let sum = 0;
            for(let j of this.students[i].marks){
                sum+=j;
            }
            totalSum += sum / this.students[i].marks.length;
            console.log(totalSum)
            count++;
        }
        return Number(totalSum/count).toFixed(1);
    }
}



let studentArr = [
    new Student(
        "Katya", "Petrova", [100,90,100]
    ),
    new Student(
        "Petro", "Petrenko", [60,73,87]
    ),
    new Student(
        "Anastasia", "Golovchenko", [80,100,75]
    )
    ];

let styleTable = new StylesTable(studentArr);

let studentsTable = styleTable.getAvg();
document.body.appendChild(studentsTable);

let totalAvgLabel = document.createElement("p");
totalAvgLabel.innerHTML = "Середній бал по групі = " + styleTable.getTotalAvg();
document.body.appendChild(totalAvgLabel);

