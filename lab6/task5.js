class Shape{
    static total = 100;

    constructor(length) {
        this.a = length;
    }
    static fill(){
        Shape.total = 100;
    }
    draw(){
        let block = document.createElement('td');
        block.style.width = this.a+"px";
        block.style.height = this.a+"px";
        block.style.opacity = Shape.total+"%";
        Shape.total -= 10;
        return block;
    }
}

let drawButton = document.querySelector("input[value = draw]");
let fillButton = document.querySelector("input[value = fill]");

drawButton.addEventListener('click', ()=>{
    let shape = new Shape(50);
    document.querySelector('tr').appendChild(shape.draw());
});

fillButton.addEventListener('click', ()=>{
   Shape.fill();
});