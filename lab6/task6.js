let addPhotoButton = document.querySelector("input[type=button]");
let container = document.querySelector(".container");

let count = 0;
container.appendChild(document.createElement('tr'));
addPhotoButton.addEventListener('click', ()=>{
    fetch("https://dog.ceo/api/breeds/image/random")
       .then(response => response.json())
       .then((data)=>{
           let image = document.createElement("img");
           image.src = data["message"];
           if(container.lastChild.childNodes.length<4) {
               container.lastChild.appendChild(image);
           }
           else{
               container.appendChild(document.createElement('tr'));
               container.lastChild.appendChild(image);
           }
       });
});